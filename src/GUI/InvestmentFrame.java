package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;


public class InvestmentFrame extends JFrame
{    
   private static final double DEFAULT_RATE = 5;
   private static final double INITIAL_BALANCE = 1000;
private static final String FIELD_WIDTH = null;   

   private JLabel rateLabel;
   JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   private BankAccount account;
   
  
   public InvestmentFrame()
   {  
      account = new BankAccount(INITIAL_BALANCE);
      resultLabel = new JLabel("balance: " + account.getBalance());
      rateLabel = new JLabel("Interest Rate:  ");
      
      
      createPanel();
      createTextField();
      
      
      setVisible(true);
   }

   public void setResult(String balance){
	   resultLabel.setText("balance:"+ balance);
   }
  public String getrateField(){
	  return rateField.getText();
  }
   public void createTextField()
   {
      
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
      final int FIELD_WIDTH = 10;
   }
   
   public void createButton(ActionListener list)
   {
     button.addActionListener(list);
   }

   public void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
}
